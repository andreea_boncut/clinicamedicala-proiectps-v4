package com.example.demo;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TestPatientController {

    @Autowired
    private MockMvc mvc;

    @Mock
    private PatientService patientService = mock(PatientService.class);

    private PatientController patientController;

    @Test
    public void testInsertPatient() {
        Patient patient = new Patient();
        patient.setEmail("testControllerPatient@gmail.com");
        patient.setFirst_name("testController");
        patient.setLast_name("Patient");
        patientController = new PatientController(patientService);
        patientController.newPatient(patient);
        verify(patientService).insertPatient(patient);
    }
}
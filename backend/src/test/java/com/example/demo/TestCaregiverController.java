package com.example.demo;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TestCaregiverController {

    @Autowired
    private MockMvc mvc;

    @Mock
    private CaregiverService caregiverService = mock(CaregiverService.class);

    private CaregiverController caregiverController;

    @Test
    public void testInsertCaregiver() {
        Caregiver caregiver = new Caregiver();
        caregiver.setEmail("testControllerCaregiver@gmail.com");
        caregiver.setFirst_name("TestController");
        caregiver.setLast_name("Caregiver");
        caregiverController = new CaregiverController(caregiverService);
        caregiverController.newCaregiver(caregiver);
        verify(caregiverService).insertCaregiver(caregiver);
    }
}
package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestDoctorService {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DoctorService doctorService;

    public void testInsertDoctor() {

        Doctor doctor = new Doctor();
        doctor.setEmail("testDoctor@gmail.com");
        doctor.setFirst_name("Test");
        doctor.setLast_name("Doctor");

        Doctor savedDoctor = doctorService.insertDoctor(doctor);

        Doctor existDoctor = entityManager.find(Doctor.class, savedDoctor.getId());

        assertThat(doctor.getEmail()).isEqualTo(existDoctor.getEmail());

    }
}

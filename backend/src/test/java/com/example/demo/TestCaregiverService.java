package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestCaregiverService {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CaregiverService caregiverService;

    public void testInsertCaregiver() {

        Caregiver caregiver = new Caregiver();
        caregiver.setEmail("testCaregiver@gmail.com");
        caregiver.setFirst_name("Test");
        caregiver.setLast_name("Caregiver");

        Caregiver savedCaregiver = caregiverService.insertCaregiver(caregiver);

        Caregiver existCaregiver = entityManager.find(Caregiver.class, savedCaregiver.getId());

        assertThat(caregiver.getEmail()).isEqualTo(existCaregiver.getEmail());

    }
}

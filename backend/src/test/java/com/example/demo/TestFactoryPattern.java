package com.example.demo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestFactoryPattern {

    @Test
    public void testCreateCaregiver(){
        UserFactory userFactory = new UserFactory();
        User caregiver1 =  userFactory.getUser("Caregiver", "Caregiver1", "factoryTest","asd@yahoo.com");
        //System.out.println(caregiver1.toString());
        assertTrue(caregiver1 instanceof Caregiver);
    }

    @Test
    public void testCreatePatient(){
        UserFactory userFactory = new UserFactory();
        User patient1 =  userFactory.getUser("Patient", "Patient1", "factoryTest","asd@yahoo.com");
        //System.out.println(caregiver1.toString());
        assertTrue(patient1 instanceof Patient);
    }
}

package com.example.demo;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TestDoctorController {

    @Autowired
    private MockMvc mvc;

    @Mock
    private DoctorService doctorService = mock(DoctorService.class);

    private DoctorController doctorController;

    @Test
    public void testInsertDoctor() {
        Doctor doctor = new Doctor();
        doctor.setEmail("testDoctorController@gmail.com");
        doctor.setFirst_name("TestController");
        doctor.setLast_name("Doctor");
        doctorController = new DoctorController(doctorService);
        doctorController.newDoctor(doctor);
        verify(doctorService).insertDoctor(doctor);
    }

    /*@Test
    public void testGetAllDoctors() {
        Doctor doctor = new Doctor();
        doctor.setEmail("ravikumar@gmail.com");
        doctor.setFirst_name("Ravi");
        doctor.setLast_name("Kumar");

        List<Doctor> allDoctors = Arrays.asList(doctor);

        given(doctorService.all()).willReturn(allDoctors);

        mvc.perform(get("/doctors")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[1].name", is(doctor.getFirst_name())));


    }*/






}

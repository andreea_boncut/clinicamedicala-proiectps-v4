package com.example.demo;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestNotifyObserver {
    @Mock
    private PatientService patientService = mock(PatientService.class);

    private PatientController patientController;



    @Test
    public void TestNotifyObserver() {

        Patient patient = new Patient();
        patient.setEmail("testControllerPatient@gmail.com");
        patient.setFirst_name("testController");
        patient.setLast_name("Patient");
        patient.setMedication("med2");
        patientController = new PatientController(patientService);
        patientController.addMedication(patient, patient.getId());
        verify(patientService).addMedication(patient, patient.getId());
    }
}

package com.example.demo;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TestUpdateObserver {

    @Autowired
    private MockMvc mvc;

    @Mock
    private PatientService patientService = mock(PatientService.class);


    @Mock
    private Observer caregiver= mock(Observer.class);


    @InjectMocks
    private PatientController patientController= new PatientController(patientService);


    private Patient patient = new Patient();

    @Test
    public void TestNotifyObserver() {

        long id=49;
        patient.setId(id);
        patient.setEmail("testControllerPatient@gmail.com");
        patient.setFirst_name("Peter");
        patient.setLast_name("John");
        patient.setMedication("algocalmin");
        patientController.addMedication(patient, id);
        //Mockito.verify(caregiver, atLeastOnce()).update(patient);
    }
}

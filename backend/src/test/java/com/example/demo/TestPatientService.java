package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestPatientService {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PatientService patientService;

    public void testInsertPatient() {

        Patient patient = new Patient();
        patient.setEmail("testPatient@gmail.com");
        patient.setFirst_name("Test");
        patient.setLast_name("Patient");

        Patient savedPatient = patientService.insertPatient(patient);

        Patient existPatient = entityManager.find(Patient.class, savedPatient.getId());

        assertThat(patient.getEmail()).isEqualTo(existPatient.getEmail());

    }
}

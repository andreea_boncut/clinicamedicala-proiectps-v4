package com.example.demo;
/**
 * exceptie folosita in cazul in care nu se gaseste pacientul cautat
 */
public class PatientNotFoundException extends RuntimeException {

    public PatientNotFoundException(Long id) {
        super("Could not find patient " + id);
    }
}
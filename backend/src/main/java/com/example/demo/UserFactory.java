package com.example.demo;

/**
 * Clasa UserFactory ajuta la crearea noilor instante de Caregiver sau Patient
 */
public class UserFactory {

    //use getShape method to get object of type shape
    public User getUser(String userType, String firstName, String lastName, String email){
        if(userType == null){
            return null;
        }
        /**
         * In cazul in care parametrul este CAREGIVER, se creeaza o instanta de asistent
         * In cazul in care parametrul este PATIENT, se creeaza o instanta de pacient
         */
        if(userType.equalsIgnoreCase("CAREGIVER")){
            Caregiver c = new Caregiver();
            c.setFirst_name(firstName);
            c.setLast_name(lastName);
            c.setEmail(email);
            return c;

        } else if(userType.equalsIgnoreCase("PATIENT")){
            Patient p = new Patient();
            p.setFirst_name(firstName);
            p.setLast_name(lastName);
            p.setEmail(email);
            return p;

        }

        return null;
    }

}

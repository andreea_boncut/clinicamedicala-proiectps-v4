package com.example.demo;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@RestController
class PatientController {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST","PUT", "DELETE");


            }
        };
    }


    @Autowired
    private final PatientService patientService;

    PatientController( PatientService patientService) {

        this.patientService = patientService;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/patients")
    List<Patient> all() {
        return patientService.all();
    }
    // end::get-aggregate-root[]

    @PostMapping("/patients")
    Patient newPatient(@RequestBody Patient newPatient) {
        return patientService.insertPatient(newPatient);
    }

    // Single item

    @GetMapping("/patients/{id}")
    Patient one(@PathVariable Long id) {
        return patientService.one(id);
    }

    @PutMapping("/patients/{id}")
    Patient replacePatient(@RequestBody Patient newPatient, @PathVariable Long id) {

        return patientService.replacePatient(newPatient, id);

    }

    @PatchMapping("/patients/{id}")
    Patient addMedication(@RequestBody Patient patient, @PathVariable Long id){
        return patientService.addMedication(patient, id);
    }

    @DeleteMapping("/patients/{id}")
    void deletePatient(@PathVariable Long id) {
        patientService.deletePatient(id);

    }
}

package com.example.demo;

/**
 * interfata implementata in clasa Patient
 */
public interface Subject {
    //public void registerObserver(Observer observer);
   // public void removeObserver(Observer observer);
    public void notifyObserver();
}
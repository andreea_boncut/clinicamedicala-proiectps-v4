package com.example.demo;
import org.mockito.Mock;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Clasa Caregiver reprezinta tabela asistentilor din clinica medicala
 * Aceasta implementeaza interfetele Observer si DisplayElement
 * Cand pacientului i se adauga/modifica medicatia (field-ul medication din baza de date),
 * asistentul este notificat
 */
@Entity
public class Caregiver implements Observer, DisplayElement, User {

    @Mock
    private transient Patient patientObs;
    private @Id @GeneratedValue Long id;
    private String first_name;
    private String last_name;
    private String email;

    public Caregiver() {}

    public Caregiver(String first_name, String last_name, String email,Patient patientObs){

        this.first_name=first_name;
        this.last_name=last_name;
        this.email=email;
        this.patientObs=patientObs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Caregiver))
            return false;
        Caregiver caregiver = (Caregiver) o;
        return Objects.equals(this.id, caregiver.id) && Objects.equals(this.first_name, caregiver.first_name)
                && Objects.equals(this.last_name, caregiver.last_name)
                && Objects.equals(this.email, caregiver.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.first_name, this.last_name, this.email);
    }

    @Override
    public String toString() {
        return "Caregiver{" + "id=" + this.id + ", first name='" + this.first_name + '\'' + ", last_name='" + this.last_name + '\'' + ", email='"+ this.email+ '}';
    }

    public void update(Patient patient) {

        display(patient);
    }

    /**
     * Functia display afiseaza un mesaj atunci cand asistentul este notificat ca pacientului i s-au adaugat medicamente noi in planul de tratament
     * @param patient
     */
    public void display(Patient patient) {
        System.out.println("ASISTENTUL a fost notificat ca tratamentul pentru pacientul "+patient.getFirst_name()+" "+patient.getLast_name()+" cu ID "+patient.getId()+" a fost schimbat: "+patient.getMedication());
}


}
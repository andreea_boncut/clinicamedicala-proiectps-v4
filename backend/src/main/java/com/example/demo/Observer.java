package com.example.demo;

/**
 * Interfata Observer implementata in clasa Caregiver
 */
public interface Observer {
    public void update(Patient patient);
}
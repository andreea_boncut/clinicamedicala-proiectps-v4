package com.example.demo;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200" )
@RestController
/**
 * Clasa CaregiverController foloseste metodele implementate in Clasa caregiverService pentru a face insert, get, delete, etc
 */
class CaregiverController {

    @Autowired
    private final CaregiverService caregiverService;

    CaregiverController( CaregiverService caregiverService) {

        this.caregiverService = caregiverService;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/caregivers")
    List<Caregiver> all() {
        return caregiverService.all();
    }
    // end::get-aggregate-root[]

    @PostMapping("/caregivers")
    Caregiver newCaregiver(@RequestBody Caregiver newCaregiver) {
        return caregiverService.insertCaregiver(newCaregiver);
    }

    // Single item

    @GetMapping("/caregivers/{id}")
    Caregiver one(@PathVariable Long id) {
        return caregiverService.one(id);
    }

    @PutMapping("/caregivers/{id}")
    Caregiver replaceCaregiver(@RequestBody Caregiver newCaregiver, @PathVariable Long id) {

        return caregiverService.replaceCaregiver(newCaregiver, id);

    }

    @DeleteMapping("/caregivers/{id}")
    void deleteCaregiver(@PathVariable Long id) {
        caregiverService.deleteCaregiver(id);
    }
}

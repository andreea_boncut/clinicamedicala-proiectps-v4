package com.example.demo;

/**
 * exceptie folosita in cazul in care nu se gaseste doctorul cautat
 */
public class DoctorNotFoundException extends RuntimeException {

    public DoctorNotFoundException(Long id) {
        super("Could not find doctor " + id);
    }
}
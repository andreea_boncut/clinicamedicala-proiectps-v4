package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Clasa DoctorService contine metodele pentru insert, getAll, getById, patch, delete
 */
@Component
public class DoctorService {
    @Autowired
    private DoctorRepository doctorRepository;

    public DoctorService(DoctorRepository doctorRepository){
        this.doctorRepository=doctorRepository;
    }

    public Doctor insertDoctor(Doctor doctor){
        return doctorRepository.save(doctor);
    }

    List<Doctor> all() {
        return doctorRepository.findAll();
    }

    Doctor one(@PathVariable Long id) {

        return doctorRepository.findById(id)
                .orElseThrow(() -> new DoctorNotFoundException(id));
    }

    Doctor replaceDoctor(@RequestBody Doctor newDoctor, @PathVariable Long id) {

        return doctorRepository.findById(id)
                .map(doctor -> {
                    doctor.setFirst_name(newDoctor.getFirst_name());
                    doctor.setLast_name(newDoctor.getLast_name());
                    doctor.setEmail(newDoctor.getEmail());
                    return doctorRepository.save(doctor);
                })
                .orElseGet(() -> {
                    newDoctor.setId(id);
                    return doctorRepository.save(newDoctor);
                });
    }

    void deleteDoctor(@PathVariable Long id) {
        doctorRepository.deleteById(id);
    }
}

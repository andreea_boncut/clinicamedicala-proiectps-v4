package com.example.demo;

public interface DisplayElement {
    public void display(Patient patient);
}
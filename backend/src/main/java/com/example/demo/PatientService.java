package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
/**
 * Clasa PatientService contine metodele pentru insert, getAll, getById, patch, delete
 */
@Component
public class PatientService  {
    @Autowired
    private PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository){
        this.patientRepository=patientRepository;
    }

    public Patient insertPatient(Patient patient){
        return patientRepository.save(patient);
    }

    List<Patient> all() {
        return patientRepository.findAll();
    }

    Patient one(@PathVariable Long id) {

        return patientRepository.findById(id)
                .orElseThrow(() -> new PatientNotFoundException(id));
    }

    Patient replacePatient(@RequestBody Patient newPatient, @PathVariable Long id) {

        return patientRepository.findById(id)
                .map(patient -> {
                    patient.setFirst_name(newPatient.getFirst_name());
                    patient.setLast_name(newPatient.getLast_name());
                    patient.setEmail(newPatient.getEmail());
                    return patientRepository.save(patient);
                })
                .orElseGet(() -> {
                    newPatient.setId(id);
                    return patientRepository.save(newPatient);
                });
    }

    Patient addMedication(@RequestBody Patient patient2 , @PathVariable Long id){
        Patient patient=patientRepository.getOne(id);
        patient.setMedication(patient2.getMedication());
        return patientRepository.save(patient);
    }

    void deletePatient(@PathVariable Long id) {
        patientRepository.deleteById(id);
    }
}

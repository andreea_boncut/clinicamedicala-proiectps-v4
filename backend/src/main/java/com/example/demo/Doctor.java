package com.example.demo;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * Clasa Doctor reprezinta tabela doctorilor din clinica medicala
 * In aplicatia finala, doctorul va avea rol de admin principal
 */

@Entity
public class Doctor {

    private @Id @GeneratedValue Long id;
    private String first_name;
    private String last_name;
    private String email;

    public Doctor() {}

    public Doctor(String first_name, String last_name, String email){

        this.first_name=first_name;
        this.last_name=last_name;
        this.email=email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Doctor))
            return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(this.id, doctor.id) && Objects.equals(this.first_name, doctor.first_name)
                && Objects.equals(this.last_name, doctor.last_name)
                && Objects.equals(this.email, doctor.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.first_name, this.last_name, this.email);
    }

    @Override
    public String toString() {
        return "Doctor{" + "id=" + this.id + ", first name='" + this.first_name + '\'' + ", last_name='" + this.last_name + '\'' + ", email='"+ this.email+ '}';
    }

}
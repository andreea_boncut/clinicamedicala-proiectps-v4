package com.example.demo;

/**
 * exceptie folosita in cazul in care nu se gaseste asistentul cautat
 */
public class CaregiverNotFoundException extends RuntimeException {

    public CaregiverNotFoundException(Long id) {
        super("Could not find caregiver " + id);
    }
}
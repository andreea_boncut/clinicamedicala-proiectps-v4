package com.example.demo;
import java.util.ArrayList;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Clasa Patient reprezinta tabela pacientilor din clinica medicala
 * Aceasta implementeaza interfata Subject
 * Cand pacientului i se adauga/modifica medicatia (field-ul medication din baza de date),
 * asistentul este notificat
 */

@Entity
public class Patient implements Subject, User {

    private transient Observer observer=new Caregiver();
    private @Id @GeneratedValue Long id;
    private String first_name;
    private String last_name;
    private String email;
    private String medication;

    public Patient() {}

    public Patient(String first_name, String last_name, String email, String medication){

        this.first_name=first_name;
        this.last_name=last_name;
        this.email=email;
        this.medication=medication;
    }

    /**
     * Adaugarea unui nou observer
     * @param o
     */
    /*
    public void registerObserver(Observer o) {
        this.observer=o;
    }

    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0) {
            observers.remove(i);
        }
    }*/

    /**
     * notificarea observerului
     */
    public void notifyObserver() {

            observer.update(this);

    }

    /**
     * functie apelata in momentul in care se face update la planul de tratament
     */
    public void medicationChanged() {
        notifyObserver();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMedication() { return medication; }

    public void setMedication(String medication) {
        this.medication = medication;
        if(this.id!=null && this.email!=null && this.first_name!=null && this.last_name!=null && this.medication!=null)
            {
                medicationChanged();
            }

         }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Patient))
            return false;
        Patient patient = (Patient) o;
        return Objects.equals(this.id, patient.id) && Objects.equals(this.first_name, patient.first_name)
                && Objects.equals(this.last_name, patient.last_name)
                && Objects.equals(this.email, patient.email)&& Objects.equals(this.medication, patient.medication);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.first_name, this.last_name, this.email, this.medication);
    }

    @Override
    public String toString() {
        return "Patient: " + "id=" + this.id + ", first name='" + this.first_name + '\'' + ", last_name='" + this.last_name + '\'' + ", email='"+ this.email+ '}';
    }

}
package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Clasa CaregiverService contine metodele pentru insert, getAll, getById, patch, delete
 */
@Component
public class CaregiverService {
    @Autowired
    private CaregiverRepository caregiverRepository;

    public CaregiverService(CaregiverRepository caregiverRepository){
        this.caregiverRepository=caregiverRepository;
    } 

    public Caregiver insertCaregiver(Caregiver caregiver){
        return caregiverRepository.save(caregiver);
    }

    List<Caregiver> all() {
        return caregiverRepository.findAll();
    }

    Caregiver one(@PathVariable Long id) {

        return caregiverRepository.findById(id)
                .orElseThrow(() -> new CaregiverNotFoundException(id));
    }

    Caregiver replaceCaregiver(@RequestBody Caregiver newCaregiver, @PathVariable Long id) {

        return caregiverRepository.findById(id)
                .map(caregiver -> {
                    caregiver.setFirst_name(newCaregiver.getFirst_name());
                    caregiver.setLast_name(newCaregiver.getLast_name());
                    caregiver.setEmail(newCaregiver.getEmail());
                    return caregiverRepository.save(caregiver);
                })
                .orElseGet(() -> {
                    newCaregiver.setId(id);
                    return caregiverRepository.save(newCaregiver);
                });
    }

    void deleteCaregiver(@PathVariable Long id) {
        caregiverRepository.deleteById(id);
    }
}

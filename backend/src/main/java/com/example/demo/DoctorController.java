package com.example.demo;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * Clasa DoctorController foloseste metodele implementate in Clasa DoctorService pentru a face insert, get, delete, etc
 */
@CrossOrigin(origins = "http://localhost:4200" )
@RestController

class DoctorController {

    @Autowired
    private final DoctorService doctorService;

    DoctorController( DoctorService doctorService) {

        this.doctorService = doctorService;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/doctors")
    List<Doctor> all() {
        return doctorService.all();
    }
    // end::get-aggregate-root[]

    @PostMapping("/doctors")
    Doctor newDoctor(@RequestBody Doctor newDoctor) {
         return doctorService.insertDoctor(newDoctor);
    }

    // Single item

    @GetMapping("/doctors/{id}")
    Doctor one(@PathVariable Long id) {
        return doctorService.one(id);
    }

    @PutMapping("/doctors/{id}")
    Doctor replaceDoctor(@RequestBody Doctor newDoctor, @PathVariable Long id) {

        return doctorService.replaceDoctor(newDoctor, id);

    }

    @DeleteMapping("/doctors/{id}")
    void deleteDoctor(@PathVariable Long id) {
        doctorService.deleteDoctor(id);
    }
}

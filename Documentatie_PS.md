# Clinica medicala
##### Boncut Andreea
##### Grupa 30239

Aplicatia reprezinta o clinica medicala online. Utilizatorii in cadrul acestei aplicatii sunt de 3 tipuri:
-	Doctor: are capacitatea de a manipula datele atat a asistentilor cat si a pacientilor si a medicamentelor
-	Pacient : drepturile acestuia sunt limitate la : vizualizarea planului medical si administrarea propriului cont 
-	Asistent: are dreptul de a vizualiza pacientii asociati si planul medical care le corespunde


### Functionalitati
##### 1. Creare plan medical: 


      Doctorul va putea crea un plan medical pentru fiecare pacient in care va trece lista medicamentelor de care are nevoie, intervalul la care se vor administra zilnic si perioada tratamentului.
##### 2. Gestionare pacienti: 


    Doctorul va putea adauga/sterge datele unui pacient din baza de date a clinicii. 


##### 3. Vizualizare/modificare date cont

    Fiecare pacient isi va adauga cateva informatii personale in cont. Acesta isi va introduce numele, prenumele, data nasterii, gen, adresa, si istoricul medical. 


##### 4. Vizualizare/modificare date cont

    Asistentul va putea vizualiza lista pacientilor pe care ii are in ingrijire, iar pentru fiecare dintre ei, va putea vizualiza planul de tratament.
    
## Tema 1
##### Pentru tema 1 am realizat:
- integrare Spring Boot
- conexiune la Baza de Date
- call-uri Get, Post, Delete
- cateva Unit Tests
- generare JavaDoc
- documentatie MD


## Tema 2
#### Observer Design Pattern
In momentul in care se adauga/ se face update la field-ul medication din tabela Pacient, asistentu primeste o notificare cu toate datele pacientului pentru care s-a realizat modificarea.
Am folosit interfetele Observer si Subject. Clasa Patient implementeaza interfata Subject cu metodele notifyObserver() si medicationChanged(). Clasa Caregiver implementeaza interfata Observer cu metoda update(). Se va afisa in consola un mesaj unde se observa ca asistentul a fost notificat despre schimbarea planului de tratament a pacientului.
![Diagrama UML](https://bitbucket.org/andreea_boncut/clinicamedicala-proiectps-v4/raw/8b642ef9c2f1efa3d287c095dbe12ce7e723042f/ObserverPatternUML.png)
![Deployment Diagram](https://bitbucket.org/andreea_boncut/clinicamedicala-proiectps-v4/raw/8b642ef9c2f1efa3d287c095dbe12ce7e723042f/DeploymentDiagram.png)

#### Endpoints list
##### Doctors
######  Post /doctors
{
    "id": 49,
    "first_name": "Ana",
    "last_name": "Maria",
    "email": "email@yahoo.com"
}

######  Get /doctors
######  Get /doctors/49
######  Patch /doctors/49

{
    "id": 49,
    "first_name": "EditedName",
    "last_name": "Maria",
    "email": "email@yahoo.com"
}
######  Delete /doctors/49

##### Patients
######  Post /patients
{
    "id": 49,
    "first_name": "Ana",
    "last_name": "Maria",
    "email": "email@yahoo.com",
    "medication": "medicationTest"
}

######  Get /patients
######  Get /patients/49
######  Patch /patients/49

{
    "id": 49,
    "first_name": "EditedName",
    "last_name": "Maria",
    "email": "email@yahoo.com",
    "medication": "EditedMedication"
}
######  Delete /patients/49

##### Caregivers
######  Post /caregiver
{
    "id": 49,
    "first_name": "Ana",
    "last_name": "Maria",
    "email": "email@yahoo.com"
}

######  Get /caregiver
######  Get /caregiver/49
######  Patch /caregiver/49

{
    "id": 49,
    "first_name": "EditedName",
    "last_name": "Maria",
    "email": "email@yahoo.com"
}
######  Delete /caregiver/49


## Tema 3
#### Factory Design Pattern
In aplicatia mea, doctorul are rol de admin si poate adauga 2 tipuri de useri: Caregiver si Patient. Am implementat interfata User cu metoda toString, pe care o extind clasele Patient si Caregiver. De asemenea, am creat clasa UserFactory cu metoda getUser, care creeaza un nou user in functie de string-ul trimis ca parametru: "Patient" sau "Caregiver".
#### Diagrama Factory
![Diagrama Factory](https://bitbucket.org/andreea_boncut/clinicamedicala-proiectps-v4/raw/e47d5b418da551a66f9a0e2b11d3e23d6b308915/FactoryPatternDiagram.png)
#### Diagrama de secventa
![Diagrama de secventa](https://bitbucket.org/andreea_boncut/clinicamedicala-proiectps-v4/raw/460bc0f8d0e953ae7e2a8b75d4758f2853c14085/SequenceDiagram.png)

## Tema 4
#### Frontend
Partea de Frontend a proiectului meu este realizata in framework-ul Angular. Am creat 6 componente noi: Navbar, Home, Patient, Caregiver, Doctor si Footer. In fisierul HTML al aplicatiei, Navbar si Footer sunt fixate, iar intre ele se afla pagina pe care userul doreste sa navigheze. 

-	Doctor: are capacitatea de a manipula datele atat a asistentilor cat si a pacientilor si a medicamentelor. Pentru a afisa toti medicii disponibili in clinica in tabela, am facut un call de getAll(). Pentru a sterge un anumit medic din tabela, am facut un call de deleteDoctor(), apoi am dat refresh la lista folosind din nou getAll().

-	Pacient : In dreapta fiecarui pacient, in tabela, se afla un buton de Delete. Prin apasarea acestuia se sterge inregistrarea respectiva din baza de date. Pentru a actualiza tabela de pacienti dupa stergere, folosesc functia loadPatients() care face un apel de getAll(). 


-	Asistent: are dreptul de a vizualiza pacientii asociati si planul medical care le corespunde. Pentru a sterge un pacient din baza de date, se efectueaza un call de Delete(), apoi se reincarca tabela actualizata. Pentru a vizualiza toti asistentii in tabela, se face un call de getAll().




